Title: Inicio TapDrum
Date: 2020-6-26 23:30
Category: TapDrum

##TapDrum
**Proyecto para la construcción de un TapDrum semejante a otro que hay en el mercado**

La idea es replicar y mejorar este instrumento MIDI que está a la venta con un precio que creo que es excesivo para las caracteríaticas que nos ofrece.<br>
El concepto es bueno, pero muy mejorable. Por lo que se puede ver en internet, la idea no es la única ni la primera.<br>

Es un instrumento pensado para no tener que mover un set de bateria, montaje, desmontaje etc.. Creo que es un instrumento concebido para ser "agil". Pero para que sea verdaderamente "agil" adolece de ciertas características que hacen que sea solo **un poco** menos tedioso de montar. Requiere un ordenador, un programa que asigne sonidos, etc.. <br>

La idea sería que fuera tan ágil como una guitarra, enchufar y tocar.
  
![TapDrum](img/general2.png "Idea general")<br>

Quiero que no se parezca tanto al que ya hay en el mercado. Las medidas no son iguales y la posición de los pads está modificada a lo que yo entiendo que deberian estar situados.

Aquí tienes los archivos de [**Blender**](http://josema966.gitlab.io/category/tapdrum.html) para que puedas modificarlos

##Tabla comparativa

Caracteristicas|**TapDrum**|Otros
---------------|---------------|---------------
MIDI input/output|SI|SI
Salida audio|SI|NO
Pantalla OLED| 128X32|NO
Controlador MIDI|ATMega2560-16AU |?
Multiplexor|74HC4067|?
Secuenciador|SI|NO
Sintetizador|FluidSynth|NO
Banco de sonidos|SF2/SF3|NO
Microprocesador|Cortex A7|?
Numero de Nucleos|4|?

##Prevision de materiales a utilizar
*Tabla de 80x40X1'8 cm 
*Pads impresos en 3D
*Orange Pi PC+
*Arduino Nano
*Multiplexador
*Convertidor USB-MIDI
*Potenciometros


### Atención.
***Si eres diseñador y quieres participar en el proyecto, puedes echarme una mano con el diseño del mismo. A cambio te enviaré materiales e información de primera mano. Así como asistencia tecnica prioritara. Formarás parte del grupo de desarrollo***

