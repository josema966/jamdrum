/***
 *  LEE PADS
 */
 
void leePads(){ // Cuidado! el pad 0 no lo lee por hard asi como el 16 que lo mismo si puede leerlo. Hay que concretar el hard
    int nuevaFuerza = 0;
    for (int a=0;a<NUMPADS;a++){   //Para leer todos los pads
      PORTD= controlPins[a];              // Selecciono el pad que quiero leer
      fuerza = analogRead(0);  // Leo lo que hay
      if (fuerza>umbral){ // Si hay señal
        Serial.print("hay señal ");
        Serial.print(fuerza);
        for (int b=1;b<lecturas;b++){   // Lo leo las pasadas que esté configurado
        nuevaFuerza = analogRead(0);
        if (nuevaFuerza>fuerza){ fuerza=nuevaFuerza;} //Tomo el valor máximo
      }
     
     fuerza=fuerza/4; // Vaya, la ecuacion coge los valores de 256... ooohhhh la fuerza es hasta 1024
     fuerza=corrige(fuerza); // Ya esta, se corrige y punto
     pad=a; // Donde se ha parado el bucle
     playNota(nota[0][pad], nota[1][pad],fuerza); // Envia, canal, nota y fuerza
     pintaDatos(0);// 0 no resalta ningun dato
    }
  }
};

/***
 * LEE POTES
 */
 
void leePotes(){
  leeVolumen();
  leePan();
};

/*****
 * LEE VOLUMEN
 */
 
void leeVolumen(){
  //Serial.println("estoy en leevolumen");
  int vol = analogRead(PVOL); // Leo valor bruto (hasta 1024)
  vol=int(vol/10.24); // lo paso a 100
  if (abs(volumen-vol)>5){
      volumen=vol;
      pintaVolumen(int(volumen));
      Serial.print("gain ");
      Serial.println(volumen/20);
      delay(100); // Por si acaso no se ve bien el logo. Se supone que el menu no cambia y que repintará el menu ¿?
  }
};

/****
 * LEE PAN
 */
 
void leePan(){
  int pan = analogRead(PPAN); // Leo valor bruto (hasta 1024)
  pan=int(pan/10.24); // lo paso a 100
  if (abs(balance-pan)>5){
      balance=pan;
      pintaPan(int(balance));
      Serial.print("pan ");
      Serial.println(balance/20);
      delay(100); // Por si acaso no se ve bien el logo. Se supone que el menu no cambia y que repintará el menu ¿?
  }
};

/***
 * LEE JOYSTICK
 */
 
void leeJoystick(byte *puntero){
  // Primero nos movemos por el menu con izquierda/derecha
  if (digitalRead(DOWN)){
    Serial.println("Leido Down");
    menu = menu +1;
    if (menu>MAXMENU) menu=0;
    repinta = true;
    delay(REBOTE); 
  }
  if (digitalRead(UP)){
    Serial.println("Leido UP");
    menu = menu -1;
    repinta=true;
    if (menu<0) menu=MAXMENU; // Ojo, que lo mismo el entero no se hace negativo
    delay(REBOTE); 
  }
  
  // Ahora nos movemos arriba abajo
  if (digitalRead(RIGHT)){
    Serial.println("Leido right");
    *puntero=*puntero - 1;
    //Serial.print("Puntero ");
    //Serial.print(*puntero);
    repinta=true;
    delay(REBOTE); 
  }
  
  if (digitalRead(LEFT)){
    Serial.println("Leido left");
    *puntero = *puntero + 1;
    //Serial.print("Puntero ");
    //Serial.print(*puntero);
    repinta = true;
    delay(REBOTE); 
  }
   
  if (digitalRead(MID)){
    Serial.println("Leido MID");
    seleccionado = true;
    Serial.print("Seleccionado =");
    Serial.println(seleccionado);
    //repinta=true; //Este repinta me va a dar dolor de cabeza. repinta dos veces el menu !! pero no repinta el submenu
    repinta=false; // Esto hay que verlo bien porque pinta dos veces cuando se selecciona algo en el menu
    delay(REBOTE); 
  }
  //Serial.print("menu ");
  //Serial.println(menu);
 };
