
byte controlPins[]={ // Por esto estan cruzados los cables. Echar un vistazo.// Empieza leyendo entrada 0 !!!
  B00000000,B10000000,B01000000,B11000000,
  B00100000,B10100000,B11000000,B11100000,
  B00010000,B10010000,B01010000,B11010000,
  B00110000,B10110000,B01110000,B11110000};
  

void playNota(byte c,byte n ,byte f){ // ANTENCION cambio esto para que sea directamente, canal, nota y fuerza
    Serial.println("Entro en playNota");
     Serial.print("noteon ");
     Serial.print(c-1); // El Fluidsynth utiliza del 0 al 15. El canal lo coge de la configuración del pad, por lo que se trabaja de 1 a 16.  
     Serial.print(" ");
     Serial.print(n);
     Serial.print(" ");
     Serial.println(f);// El arduino lee hasta 1024
};

 
int calculaFactores(){ // Factores van de -3 a 3, pero la variable de 0 a 8 al ser byte (no admite negativos)
    fa = -0.000845*(factor-4)/4-0.000015;
    fb= 0.2433*(factor-4)/4+0.5067;
    fc=-3.3385*(factor-4)/4-0.5965;
}

int corrige(int vx){
  return(fa*vx*vx+fb*vx+fc);
  
};


/************
 * funciones del menu
 */
 /***
  * ESTO QUEDA OBSOLETO
  */
  /*
 void seleccionaFuentes(){ // Pintar las flechas bien ya que conozco tamaño del indice
    submenu=0;
    indiceFuente=0;
    seleccionado=false; 
    repinta=true;
    while (!seleccionado){   
      pintaSubMenu(fuentes[indiceFuente]);
      leeJoystick(&indiceFuente);
      if(menu!=1) return; //Es decir si cambio de menu, es porque no quiero cargar la fuente
      if(indiceFuente>250) indiceFuente=0;
      if(indiceFuente>numFuentes-1) indiceFuente=numFuentes-1;// El array va desde 0 a numFuentes-1
      //if(submenu>4) submenu=4;
      // repinta=true; viene del repinta del dolor de cabeza arriba
      // seleccionado=false; 
    }
    Serial.print("loadfont /usr/share/sounds/sf2/");
    Serial.print(fuentes[indiceFuente]);
    Serial.println(".sf2");
    menu=0;
    submenu=0;
    repinta=true;
    seleccionado=false;
    //Serial.println(sizeof(fuentes));
 };
 */
 
 /***
  *   PADS
  */
 void setMapas(){
  seleccionado = false;
  byte indice=0;
  byte antIndice=1; // para que sean distintos y lo haga una vez
  while(!seleccionado){
    if (indice!=antIndice){
        repinta=true;
        pintaSubMenu(mapas[indice]);
        antIndice=indice;
    }
    leeJoystick(&indice); 
    if (indice>250) indice=0;
    if (indice>3) indice=3;   
  }
  leeEeprom(indice);
  
 }
 void pads(){ //Aclarar lo que pasa con los pads. Intento dejarlo que sirva para toda configuración
              // Los pads pasa como mlos canales, empiezan (y trabajamos) de 0 pero se muestran con +1
    seleccionado=false;
    pad=0;
    while(!seleccionado){ // Este primero para seleccionar el pad
      leePads(); //por si vamos más rapido tocando el pad
      leeJoystick(&pad);
      if (pad>NUMPADS-1) pad=NUMPADS-1;  //los pads van de 0 a 31, como el array que los contiene
      if (pad>250) pad= 0; // Esto es porque un byte no puede ser negativo, de 0 pasa a 255      
      pintaDatos(1);
      //Serial.println("Debió de pintar los datos");
      
    }
    seleccionado=false;
    canalSe=nota[0][pad];
    while(!seleccionado){ // Este selecciona el canal
      
      leeJoystick(&canalSe);
      if (canalSe<0) canalSe=0;
      if (canalSe>16) canalSe=16;
      if (canalSe>250) canalSe=0; // Porque un byte no puede ser negativo, del 0 pasa al 255
      nota[0][pad]=canalSe;      
      pintaDatos(2);
      //Serial.println("Debió de pintar los datos"); 
    }
    
    seleccionado=false;
    notaSe=nota[1][pad];
    while(!seleccionado){ // Este selecciona la nota
      leeJoystick(&notaSe);
      if (notaSe<36) notaSe=36;
      if (notaSe>128) notaSe=128;
      if(notaSe!=nota[1][pad]) playNota(nota[0][pad],notaSe,127); // Si ha cambiado la nota, reproducirla
      nota[1][pad]=notaSe;      
      pintaDatos(3);      
      //Serial.println("Debió de pintar los datos");
    }
  
  repinta=true;
  pintaMenu("Guardado");
  seleccionado=false; 
  menu=0;
  repinta=true;
 };

 /******
  * CANALES
  * 
  */

void confCanales(){
  canalSe=0;             //Empezamos a configurar desde 0
  byte antCanal=canalSe;
  repinta = true;
  pintaCanales(1);
  seleccionado=false;
  while(!seleccionado){ // Este selecciona el canal      
      leeJoystick(&canalSe);
      if (canalSe<0) canalSe=0;
      if (canalSe==9&&antCanal==8) canalSe=10; // Hace saltar el canal de percusion que es el 9 Pero no salta al reves"
      else if (canalSe==9&&antCanal==10) canalSe=8; // Para que tambien suba 
      // Primero tiene que ver que no sea "negativo" porque si no del 16 pasa al 0.
      if (canalSe>250) canalSe=0; // Porque un byte no puede ser negativo, del 0 pasa al 255
      if (canalSe>15) canalSe=15; // De 0 a 15 en el programa, se muestra con +1 OJO
      if(antCanal!=canalSe){
        pintaCanales(1);
        antCanal=canalSe;
      }
      //Serial.println("Debió de pintar los datos"); 
    }
    
  seleccionado=false;
  instSe=canales[canalSe].instrumento; // Leo el instrumento para ese canal, el que ya está configurado de antes.
  repinta=true;
  pintaCanales(3);
  while(!seleccionado){ // Este selecciona el instrumento
      leeJoystick(&instSe);
      if (instSe<0) instSe=0;// Los instrumentos empiezan por 1 --- creo ---
      // Primero tiene que ver que no sea "negativo" porque si no del 16 pasa al 0.
      if (instSe>250) instSe=0; // Porque un byte no puede ser negativo, del 0 pasa al 255
      if (instSe>127) instSe=127; // De 0 a 127 en el programa, ¿¿¿¿¿se muestra con +1 OJO????
      if(canales[canalSe].instrumento!=instSe){ //Si se ha modificado
        canales[canalSe].instrumento=instSe;
        pintaCanales(3);
      }
      //Serial.println("Debió de pintar los datos"); 
    }
  // Tiene que mandar la orden del canal a fluidsynth
  Serial.print("prog ");
  Serial.print(canalSe);
  Serial.print(" ");
  Serial.println(canales[canalSe].instrumento);
  
  seleccionado=false;
  menu=0;
  repinta=true;
    
};
/*****
 *  PARAMETROS
 */
 
void parametros(){
  byte antMenu=menu;
  submenu=0;
  seleccionado=false; 
  repinta=true;
  String str;
 while (!seleccionado){
    str = String(param[submenu] + " " + *punt[submenu]); 
    pintaSubMenu(str);
    leeJoystick(&submenu);
    if(menu!=antMenu) return; //Es decir si cambio de menu, es porque no quiero cargar la fuente
    if(submenu<0) submenu=0;
    if(submenu>3) submenu=3; // Ojo el numero de parametros que pueden modificarse
 };
 seleccionado=false;
 // Aqui tengo en submenu el parametro que quiero modificar
 while (!seleccionado){
  leeJoystick(punt[submenu]);
 // Serial.print("puntero ");
//  Serial.print(String(punt[submenu]));
//  Serial.print(" ");
//  Serial.print(&umbral);
//  Serial.print(" ");
//  Serial.println(*punt[submenu]);
  str = String(param[submenu] + " " + *punt[submenu]); 
 // Serial.println(str);
  pintaSubMenu(str);
 }
 
 menu=0;
 submenu=0;
 repinta=true;
 
};

/*****
 * CORRECCION
 */
 
void correccion(){
  byte antMenu=menu;  // Memoriza los datos antes de hacer nada
  pintaCurva();
  seleccionado=false;
  byte antFactor=factor;
  byte antAntFactor=factor;
  while (!seleccionado){
    
    //Serial.print("pasapor la asignacion");
    leeJoystick(&factor);
    if (factor>250) factor=0; //Me he pasado de vueltas
    if (factor>8) factor=8;
    if (factor!=antFactor){
      Serial.println(factor);
      calculaFactores();
      pintaCurva();
      antFactor=factor;
    }
    if(menu!=antMenu){
      factor=antAntFactor; // Deja el factor del principio ???
      calculaFactores(); // Para dejarlos como al principio
      repinta=true;
      return; // Ver esto como le sienta y cuando vaya bien aplicarlo donde se pueda.
    }
  }
  menu=0;
  submenu=0;
  repinta=true;
};

/***
 * GUARDAR EEPROM
 */
 
 void guardaEeprom(){
  for (int a=0;a<NUMPADS;a++){
    EEPROM.write(a*2+64*3,nota[1][a]); // El +64*3 es porque lee el banco 3 que es de la memnoria 1. Hay que hacer mas memorias.. Hasta 10!!
    EEPROM.write(a*2+1+64*3,nota[0][a]);
  }
  // Hay que guardar .. Umbral, Lecturas, Factor
  
  repinta=true;
  pintaMenu("Guardado");
  seleccionado=false; 
  menu=0;
  repinta=true;
  Serial.println("Guardada Eeprom");
 };

 /****
  * LEER EEPROM
  */
  
void leeEeprom(byte indice){
  for (int a=0;a<NUMPADS;a++){
    nota[1][a]= EEPROM.read(a*2+64*indice);
    nota[0][a]=EEPROM.read(a*2+1+64*indice);
  }
  repinta=true;
  if (menu !=0)pintaMenu("Leido");
  seleccionado=false; 
  menu=0;
  repinta=true;
  //Serial.println("Leida Eeprom");
 };
