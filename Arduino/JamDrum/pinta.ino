/***
 * PINTA LOGO
 */
 
void pintaLogo(){
  display.clearDisplay();
  display.drawBitmap(0,8,logo1_bmp,16, 17, 1);
  display.drawBitmap(16,8,logo2_bmp,16, 17, 1);
  display.drawBitmap(32,8,logo3_bmp,16, 17, 1);
  display.drawBitmap(48,8,logo4_bmp,16, 17, 1);
  display.drawBitmap(64,8,logo5_bmp,16, 17, 1);
  display.drawBitmap(80,8,logo6_bmp,16, 17, 1);
  display.drawBitmap(96,8,logo7_bmp,16, 17, 1);
  display.drawBitmap(112,8,logo8_bmp,16, 17, 1);
  display.display();
//  delay(1000);
};

/***
 * PINTA DATOS
 */
 
void pintaDatos(int dato){
/*  triangulo arriba 31
 *  triangulo abajo 32
 *  flecha izq 28
 *  flecha der 27
 *  flecha aba 26
 *  flecha arri 25
 *  flecha arr-ab 18 y 24 
 *  triangulo iz 17
 *  triangulo der 16
 *  nota musical 15
  
  display.write(30);
  display.write(31);
  display.write(17);
  display.write(16);
  display.write(24);
  display.write(25);
  display.write(27);
  display.write(26);
 */
  
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE,SSD1306_BLACK);
  display.setCursor(0,0);
  display.cp437(true);
  display.print("P ");
  if (dato==1) display.setTextColor(SSD1306_BLACK,SSD1306_WHITE);
  if (pad<9) display.print(" ");
  display.print(pad+1);
  //Serial.print("pad ");
  //Serial.print(pad);
  //Serial.print( "  ");
  display.setTextColor(SSD1306_WHITE,SSD1306_BLACK);
  display.setCursor(64,0);
  display.print("C  ");
  if (dato==2) display.setTextColor(SSD1306_BLACK,SSD1306_WHITE);
  if (nota[0][pad]<10) display.print(" ");
  display.print(nota[0][pad]);
  //Serial.print("canal ");
  //Serial.print(canal[pad]);
  display.setTextColor(SSD1306_WHITE,SSD1306_BLACK);
  display.setCursor(0,16);
  display.print("N ");
  if (dato==3) display.setTextColor(SSD1306_BLACK,SSD1306_WHITE);
  if (nota[1][pad]<10) display.print(" ");
  display.print(nota[1][pad]);
  //Serial.print("nota ");
  //Serial.println(nota[pad]);
  display.setTextColor(SSD1306_WHITE,SSD1306_BLACK);
  display.setCursor(64,16);
  if(dato==0){
    display.print("F ");
    if (fuerza<10) display.print(" ");
    if (fuerza<100) display.print(" ");
    display.print(fuerza);
  }      
  display.display();
};

/***
 * PINTA MENO
 */
 
void pintaMenu(String msg){  
  if(repinta){
    // Serial.println("Entro en el pintamenu");
    display.setTextColor(SSD1306_WHITE);
    display.setTextSize(2);
      for (int a=SCREEN_WIDTH/2;a>0;a=a-2){
        display.clearDisplay();
        display.setCursor(a,12);
        if(menu>1) display.write(17);
        display.print(msg);
        if (menu< MAXMENU) display.write(16);
        display.display();
      }
    } 
  repinta=false;
 };

 /****
  * PINTA SUBMENU
  */
  
void pintaSubMenu(String msg){
  if(repinta){
    // Serial.println("Entro en el pintamenu");
    // display.setTextColor(SSD1306_WHITE);
    // display.setTextSize(2);
      for (int a=SCREEN_HEIGHT;a>8;a=a-1){
        display.clearDisplay();
        display.setCursor(0,a);
        display.print(msg);
        display.write(18);
        display.display();
      }
    } 
  repinta=false;
 };

/************************
 * PINTA CANALES
 * 
 * ATENCION !!! ESTO ESTÁ INCOMPLETO. La idea era dar a cada canal un preset de un banco de una fuente. Es complicado si se cargan las fuentes desordenadas.
 * La idea posterior es, cargar todas las fuentes del tiron. Una matriz tendría la configuracion de fuente, banco y preset para cada nombre de un set de bateria.
 * Las funciones del FONT quedarian obsoletas. Las pongo entre asteriscos.
 */


 void pintaCanales(byte sel){
  // Serial.println("pintaCanales");
  if (repinta){
    display.clearDisplay();
    display.setCursor(0,0);
    display.println("Ca Ban Ins");
    if (sel==1)display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    if (canalSe<9) display.print(" ");
    display.print(canalSe+1); // Tiene que mostrar uno mas que el que lleva el programa
    display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);

    display.print(" ");
    bancoSe=canales[canalSe].banco; /// Ver esto, pero en principio bien porque no se toca
    if (sel==2)display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    if (bancoSe<10) display.print("0");
    if (bancoSe<100) display.print("0");
    display.print(bancoSe);
    display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);

    display.print(" ");
    instSe=canales[canalSe].instrumento;
    if (sel==3)display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    if (instSe<10) display.print("0");
    if (instSe<100) display.print("0");
    display.print(instSe);
    display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);
    display.display(); 
  }
 };


/****
 * PINTA VOLUMEN
 */
 
void pintaVolumen(int vol){ // el volumen se pinta de 0 a 100. Se lee del pote de 0 a 128 y se manda al fluidsynth de 0 a 5,0 (float) (es dividir por 20)
  int x;
  int y;
  seleccionado=0;
  display.clearDisplay();
  y=31 -(vol/4);
  display.fillTriangle(14,31,vol+14,31,vol+14,y,SSD1306_WHITE); //Interior volumen
  for (int a=0;a<10;a++){ //pinta las 9 rayas negras
    x=a*10+14;
    display.drawLine(x,31,x,0,SSD1306_BLACK);
    display.drawLine(x+1,31,x+1,0,SSD1306_BLACK);
  }
  display.drawTriangle(14,31,113,31,113,6,SSD1306_WHITE); // Exterior blanco
  display.setCursor(0,0); 
  display.setTextSize(2); 
  display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);
  display.print(vol);
  display.display();
  // Ver si esto hay que quitar o no
  delay(700);
  menu =0;
  submenu=0;
  repinta=true;
  
};

/****
 * PINTA PAN
 */
 
void pintaPan(int pan){ 
  int x;
  int y;
  seleccionado=0;
  display.clearDisplay();
  display.setCursor(0,0);
  display.fillRect(14,18,100,12,SSD1306_WHITE); //interior blanco
  for (int a=0;a<10;a++){ //pinta las 9 rayas negras
    x=a*10+14;
    display.drawLine(x,31,x,0,SSD1306_BLACK);
    display.drawLine(x+1,31,x+1,0,SSD1306_BLACK);
  }
  display.drawRect(12,16,104,16,SSD1306_WHITE); // Exterior blanco
  
  // Vamos con el cursor
  display.drawRect(pan+7,9,14,22,SSD1306_BLACK); // Exterior cursor negro
  display.drawRect(pan+8,10,12,21,SSD1306_WHITE); // Exterior cursor blabco
  display.drawRect(pan+9,11,10,19,SSD1306_BLACK); // Exterior cursor negro
  display.fillRect(pan+10,12,8,17,SSD1306_WHITE); // Interior cursor blanco
  
  display.setCursor(0,0);
  display.print(pan-50);
 
  display.display();
  // Ver si esto hay que quitar o no
  delay(700);
  //menu =0;
  //submenu=0;
  //repinta=true;
  
};

/*****
 * PINTA CURVA
 */
 
void pintaCurva(){
  display.clearDisplay();
  display.setCursor(0,0);
  display.drawRect(127,0,127,32,SSD1306_WHITE); // eje de las y
  display.drawRect(0,31,128,31,SSD1306_WHITE); // eje de las x
  
  for (int a=umbral; a<256;a=a+2){ // 
    display.drawPixel(int((a-umbral)/2),int((128-corrige(a))/4),SSD1306_WHITE);
    }
  display.setCursor(0,0);
  display.setTextSize(1);
  display.print(100+(factor-4)*5);
  display.print("%");
  display.display();
  //delay(5000);
};
