// Segundo programa por perdida del primero

// Carga de librerias
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>

/******************
 * atencion. voy haciendo la lectura de distintos MAPAS en ls EEPROM
 */




//define la placa
//las opciones son UNO o MEGA

#define ARDUIN 1   // 1 para Arduino UNO, 0 Para Arduino MEGA

// define constantes
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32

#define NUMPADS 16 // Los pads van del 0 al 31, pero se muestran del 1 al 32. como todo el programa
#define NUMCANALES 16 // Se pueden configurar mas canales en fluidsynth.

// Define el joystick. los nombres son los marcados en la placa 
#if ARDUIN == 1 // Estaria bien, pero ya se podria quitar, coinciden los pines en las dos placas!!
  #define UP 13
  #define DOWN 12
  #define LEFT 11
  #define RIGHT 10
  #define MID 9
  #define PVOL A1
  #define PPAN A3
  // #define PCHAR A5 // no se puede utilizar en el UNO porque lo utiliza el DISPLAY

#elif ARDUIN == 0
  #define UP 13 // pin 13 D13
  #define DOWN 12 // pin 12 D12
  #define LEFT 11// pin 11 D11
  #define RIGHT 10 // pin10 D10
  #define MID 9 // pin 9 D9
  #define PVOL A1 // pin 55 A1
  #define PPAN A3 // pin 57 A3
  #define PCHAR A5 //pin 59 A5
#endif

#define REBOTE 300 // Tiempo que espera para no hacer rebote
#define MAXMENU 7  // Cuidado con esto al ampliar el menu

// definicion de variables

byte nota[2][NUMPADS];     //Guardo el canal y la nota midi relacionada con el pad // del 0 al 31
/**********
 * lo dicho. Las fuentes las cargo todas al inicio (a ver como lo toma el fluid) y luego solo manejo los presets de cada fuente
 * 
 */
 /*****
    String fuentes[]= {"defaultGM","drums171k","FMSyntesi","Jv_drums","Nice-Bass","PNDrumKit","R-drum1","R-drum2","RealAcous","Sordrums","Syndrums","Techdrum","TimGM6mb"};
    int numFuentes = sizeof(fuentes)/6; //// VER ESTO no tiene por que se así
*/

int fuerza =0;           // Es un int porque guardo en ella la suma de los valores leidos
byte umbral = 15;        // Minima fuerza necesaria para que salte el pad.
byte lecturas =10;     // Dato a configurar
byte metodo = 0;         // metodo que se utiliza para sacar valor maximo
byte midiout = 0;        // si no es cero, saca midi. En un futuro.

float volumen;           // Será la ganancia del jam/ / el volumen se pinta de 0 a 100. Se lee del pote de 0 a 128 y se manda al fluidsynth de 0 a 5,0 (float) (es dividir por 20)
float balance;           //Lo que es el pan
byte menu;
byte submenu;
byte pad;                //contiene el pad seleccionado
byte canalSe;            // Contiene el canal seleccionado
byte bancoSe;            // Contiene el banco del midi (normalmente 0 o 128)
byte instSe;             // Instrumento seleccionado
byte preset;             // Contiene el preset dentro del banco
byte notaSe;             // Contiene la nota seleccionada
byte indiceFuente;       // El numero del indice de la fuente
struct canal{
  // el numero de canal lo da el indice del array
  byte banco;
  byte instrumento;
};
canal canales[NUMCANALES];
String mapas[]={"DrumSet","EBass","Percusion","Mem 1"};

byte factor=4;           //factor de correccion de la fuerza. Tiene que ir en Byte y por tanto en numero positivos. de 0 a 8 
float fa,fb,fc;

String param[]={"Umbral","Lectur","Factor","Metodo","MIDIou"};
byte *punt[]={&umbral,&lecturas,&factor,&metodo,&midiout};

boolean seleccionado=false;
boolean repinta = true;   //indica si se debe repintar el menu
// Inicializa la pantalla OLED
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup(){
  //Configura puerto serie
  Serial.begin(115200);
  Serial.println("OK");
 
  // Selecciona el puerto que controla el multiplexador
  DDRD=B11111111;        // Pone el puerto D de salida
  /***** 
   *En el MEGA la cosa se complica el cableado un poco
   * PD0 21 PD1(s3) 20(s2)
   * PD2 19(s1) PD3 18 (s0)
   * 
   * PD4 78 PD5 77
   * PD6 76 PD7 38
    */
  // Selecciona los pines del joystick
  pinMode(UP,INPUT);
  pinMode(DOWN,INPUT);
  pinMode(LEFT,INPUT);
  pinMode(RIGHT,INPUT);
  pinMode(MID,INPUT);

  // Configura pantalla OLED y saca error si no la encuentra (poner tal vez un error sonoro) y se queda en bucle sin continuar!!!
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  
  // Configura los valores iniciales del jam

  menu=0;
  submenu=0;
  seleccionado=false; // Debería bastar para que, cuando entre en el menu, no haga nada.. OBSERVAR ESTO
  
  // Inicia las notas y los canales
  // leeEeprom();
  calculaFactores(); // los factores de la curva para hacer la corrección
 
  for (int a=0;a<NUMPADS;a++){
    nota[0][a]= 10; // utilizo los numeros que son. para Fluidsynth van del 0 al 15 los canales ya los envio despues corregidos
    nota[1][a]= 35 + a; //Parece que el 35 empieza con el bombo    
  }
 /**
 for (int a=0;a<NUMPADS;a++){
    Serial.print("Nota ");  
    Serial.print(nota[1][a]);
    Serial.print(" ");
    Serial.print("Canal ");
    Serial.println(nota[0][a]); 
 }
****/
  // Cuando se grabe en la eeprom, quitar esto
  
  for (int a=0;a<16;a++){
    canales[a].banco=0;
    canales[a].instrumento=a+1;
  }
    canales[9].banco = 128;   // El banco de las baterias
    canales[9].instrumento=0; // Primer set de bateria 
    
  // Empieza a hacer cosas
  display.clearDisplay();
  pintaLogo();
  //delay(1000);

  //Empieza a mandar datos a la Orange Pi
  Serial.print("gain ");
  Serial.println(volumen/20);
  Serial.println("load /usr/share/sounds/sf2/FluidR3_GM.sf2"); // Esta seria la fuente 1
  Serial.println("prog 0 11"); // Pone el canal 1 (0 en programa) a vibrafono. // VER si esto queda obsoleto al cargar todos los canales configurados.
  // Serial.println(ARDUINO);  //Siempre da el mismo numero. hay que investigar en las bibliotecas
}

void loop(){
  // leePads();
  // leePotes();
  leeJoystick(&menu);
  /*
   
  Serial.print("Al principio, menu vale ");
  Serial.println(menu);
  Serial.print("Y el repinta");
  Serial.println(repinta);
  */
 switch (menu) {
   case 0:
    if(repinta){
      pintaLogo();
      repinta=false;
    };
    if (seleccionado) Serial.println("reset"); // Esto lo que hace es enviar un reset para callar todas las notas. boton del pánico.
    // Si está el logo visible y se pulsa, se calla lo que esté reproduciendo. OJO- ME DESPROGRAMA LOS CANALES. Las fuentes las mantiene.
    // setChannels(); // Serviria para con la orden PROG seleccionar un instrumento al canal.
    
    seleccionado=false; //Para que no entre en el primer menu del tirón
    break;
    
   case 1:
     pintaMenu("Mapas");
     if(seleccionado) setMapas();
     break;
     
   case 2:
     pintaMenu("Pads");
     if (seleccionado) pads();
     break;

   case 3:
    pintaMenu("Canales");
    if(seleccionado) confCanales();
    break;
     
   case 4:
     pintaMenu("Parametr");
     if(seleccionado) parametros();
     break;

   case 5:
      pintaMenu("Correccion");
      if(seleccionado) correccion();
      break;

   case 6:
     pintaMenu("Guardar");
     if(seleccionado) guardaEeprom();
     break;
     
   case 7:
     pintaMenu("Leer");
     if(seleccionado){ //leeSet();
        leeEeprom(0);
        repinta=true;
        pintaMenu("Leido");
     }
     break;
 }

}
   /***** 
    *  La carga de fuentes queda obsoleta. Se cargan las fuentes y luego se manejan los sets de bateria
    */
    /*
     pintaMenu("Fuentes");
     // Serial.println("fuentes");
     if (seleccionado) seleccionaFuentes();
     */
     
