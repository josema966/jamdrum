Title: TOTAL JamDrum
Date: 2021-3-19 14:30
Category: JamDrum
Modified: 2021-3-19 14:30
Tags: free, hardware, zendrum, jamdrum
Author: Jose Maria Martinez Garcia
Lang:sp
Status: draft


#JamDrum
**Proyecto para la construcción de un JamDrum semejante a otro que hay en el mercado**

##¿Que es un TapDrum?

Es un instrumento electrónico de percusión que se toca con los dedos. Tiene una forma que se asemeja a una guitarra (o guitarra invertida) para que sea cómodo de llevar. Dispone de unos pads, que no son otra cosa que unos sensores que detectan cuando se les ha golpeado con los dedos y la electrónica, que dispone, genera los sonidos. Se pueden encontrar estos sonidos, agrupados en bancos, libremente en internet o bien adquiriendolos.
Yo lo voy a nombrar como *JamDrum*

##¿Cual es el objetivo?
La idea es replicar y mejorar este instrumento MIDI que está a la venta con un precio que creo que es excesivo para las caracteríaticas que nos ofrece.<br>
El concepto es bueno, pero muy mejorable. Por lo que se puede ver en internet, la idea no es la única ni la primera.<br>

Es un instrumento pensado para no tener que mover un set de bateria, montaje, desmontaje etc.. Creo que es un instrumento concebido para ser "agil". Pero para que sea verdaderamente "agil" adolece de ciertas características que hacen que sea solo **un poco** menos tedioso de montar. Requiere un ordenador, un programa que asigne sonidos, etc.. <br>

La idea sería que fuera tan ágil como una guitarra, enchufar y tocar.
  
![JamDrum](img/JamDrumColor.png "Idea general")<br>

###Atención

***Si eres diseñador y quieres participar en el proyecto, puedes echarme una mano con el diseño del mismo. A cambio te enviaré materiales e información de primera mano. Así como asistencia tecnica prioritara. Formarás parte del grupo de desarrollo***

Quiero que no se parezca tanto al que ya hay en el mercado. Las medidas no son iguales y la posición de los pads está modificada a lo que yo entiendo que deberian estar situados.

Aquí tienes los archivos de [**Blender**](https://gitlab.com/josema966/jamdrum/-/tree/master/blender) para que puedas modificarlos

##Tabla comparativa

Caracteristicas|**JamDrum**|Otros
---------------|---------------|---------------
MIDI input/output|SI|SI
Salida audio|SI|NO
Pantalla OLED| 128X32|NO
Controlador MIDI|ATMega2560-16AU |?
Multiplexor|74HC4067|?
Secuenciador|SI|NO
Sintetizador|FluidSynth|NO
Banco de sonidos|SF2/SF3|NO
Microprocesador|Cortex A7|?
Numero de Nucleos|4|?

##Prevision de materiales a utilizar <br>
*Tabla de 80x40X1'8 cm <br>
*Pads impresos en 3D <br>
*Orange Pi PC+ <br>
*Arduino Nano <br>
*Multiplexador <br>
*Convertidor USB-MIDI <br>
*Potenciometros <br>


![JamDrum](img/logo.png "Idea general")<br>

***Si eres diseñador y quieres participar en el proyecto, puedes echarme una mano con el diseño del mismo. A cambio te enviaré materiales e información de primera mano. Así como asistencia tecnica prioritara. Formarás parte del grupo de desarrollo***


##Cuerpo del JamDrum

Para hacer el cuerpo del JamDrum he utilizado una tabla de 80x40x1'8 de abeto que podrás comprar en Leroy Merlin ![aqui](https://leroymerlin.es/fp/10471580/tablero-macizo-de-abeto-de-40x80x1-8-cm "Aqui")<br>

Gracias a que he dibujado previamente en 3 dimensiones usando **blender** todo el JamDrum, puedo hacer dos cortes horizontales y sacar las 4 secciones que formarán las tres tablas que, una vez encoladas, limadas y lijadas, formarán el cuerpo <br>

Aqui tienes las secciones dibujadas, solo debes pasarlas a las tablas y cortarlas. Fijate bien que hay lineas de las secciones que no debes cortar, son sólo una referencia de por donde debe ir luego la lima para desbastar
<br>
Como veis en la foto, solo he cortado la sección superior y la inferior. La sección intermedia la he hecho con listones que me han salido de los restos de la propia tabla. Así estará hueca y pesará mucho menos<br>
 
  
![JamDrum](img/JamDrumColor.png "Idea general")<br>


### Atención.
***Si eres diseñador y quieres participar en el proyecto, puedes echarme una mano con el diseño del mismo. A cambio te enviaré materiales e información de primera mano. Así como asistencia tecnica prioritara. Formarás parte del grupo de desarrollo***

Quiero que no se parezca tanto al que ya hay en el mercado. Las medidas no son iguales y la posición de los pads está modificada a lo que yo entiendo que deberian estar situados.

Aquí tienes los archivos de [**Blender**](https://gitlab.com/josema966/jamdrum/-/tree/master/blender "Blender") para que puedas modificarlos

Si bicheais por la página original del **ZenDrum** podreis ver otros modelos distintos que os pueden dar ideas de cómo realizar vuestro propio diseño. Desde un sencillo modelo paralepipédico a otro más complicado que me recuerda a un mando de videoconsola. Diseña tu propido modelo y compártelo con nosotros.

En el repositorio puedes encontrar el plano del cuerpo de madera del JamDrum. La parte superior va alineada y si te fijas en las imágenes en de Blender (tambien las tienes en .stl) desde el resto de los bordes hay que limar la madera pues va en bisel. En la parte inferior se aganda y en la parte de la pala del mastil se achica, para que se parezca a la pala del clavijero de una guitarra eléctrica. Tambien se achica por la parte inferior del cuerpo donde llevará los potenciometros.<br>

Iré poniendo fotos del proceso y de como va quedando el cuerpo.


  
![JamDrum](img/JamdrumPlanoGeneral.png "Plano general")<br>
![JamDrum](img/JamdrumPlano.png "Plano de las partes")<br>

Aquí tienes los archivos [**STL**](https://gitlab.com/josema966/jamdrum/-/tree/master/blender/Jamdrum1tabla.stl) para que puedas modificarlos

##Diseño de los pads

  
![JamDrum](img/JamDrumColor.png "Idea general")<br>


### Atención.
***Si eres diseñador y quieres participar en el proyecto, puedes echarme una mano con el diseño del mismo. A cambio te enviaré materiales e información de primera mano. Así como asistencia tecnica prioritara. Formarás parte del grupo de desarrollo***



