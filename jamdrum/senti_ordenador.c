#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

char buffer[]="noteon 9 35 127\n";
int sockfd, n;

void logosound();

int main(int argc, char argv[]){

// Valores generales
	int res;
	int sigue = 1;
	int serial_port=-1;

// Valores del puerto serie (revisar)
	char recibido[1]; //Lo que envia el puerto serie lo cogemos de uno en uno
	char buf[50]; //Lo que recibe el puerto serie y envia por el socket
	//char puerto[]="/dev/ttyACM0"; //puerto serie Arduino UNO
    char puerto[]="/dev/ttyUSB0"; //puerto serie Arduino Mega

	char p[]="0";
	char men[30];//Buffer de salida 
	bzero(&men,sizeof(men));
	bzero(&buf,sizeof(buf)); //Lo que recibe el puerto serie y envia por el socket
	bcopy("OK",&men,2);

	struct termios newtermios;

// Valores del socket
	int  puertoIp=9800; // Puerto Ip de fluidsynth
    struct sockaddr_in serv_addr;
    struct hostent *server;

// La a sirve paramirar los 5 puertos USB por si uno se bloquea
	for (int a=0;a<5;a++){
		printf("Iniciando con %s\n",puerto);	
	 	serial_port =open(puerto,O_RDWR|O_NOCTTY);

		if (serial_port>0){
			printf("Abierto el puerto : %s\n",puerto);		
			break;
		}
		else {
			puerto[11]=puerto[11]+1;
			printf("intentando con: %s\n",puerto);		
		}
	}

// Configura los parametros del serie
	tcgetattr(serial_port, &newtermios); //coge los parametros actuales
	newtermios.c_cflag=CBAUD|CS8|CLOCAL|CREAD;
	newtermios.c_iflag=IGNPAR;
	newtermios.c_oflag=0;
	newtermios.c_lflag=0;
	newtermios.c_cc[VMIN]=1;
	newtermios.c_cc[VTIME]=0;
	cfsetospeed(&newtermios,B38400);
	cfsetispeed(&newtermios,B38400);
	cfsetspeed(&newtermios,B38400);
	if(tcflush(serial_port,TCIFLUSH)==-1) return -1;
	if(tcflush(serial_port,TCOFLUSH)==-1) return -1;
	tcsetattr(serial_port, TCSANOW, &newtermios); //Para seleccionar los nuevos parametros

// Configura sockets
	// usleep(500); //Este tiempo es en milisegundos y vale para que cargue el fluidsynth.
	// no lo utilizo porque cargo primero fluidsynth
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        printf("ERROR opening socket");
    /*
    server = gethostbyname("localhost");
    
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;

    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    */

    serv_addr.sin_port = htons(puertoIp);
    serv_addr.sin_addr.s_addr = inet_addr("192.168.1.2");
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        printf("ERROR connecting");

// ***************************

	n=write(serial_port,men,2); //Dice al arduino que esta listo para recibir
	bzero(&men,sizeof(men));
	if (n < 0) 
         printf("ERROR writing to serial");
    //n = write(sockfd,buffer,strlen(buffer));
    //if (n < 0) 
    //     printf("ERROR writing to socket");
    //bzero(buffer,256);
	logosound();
	
// **** empieza la tela
	printf("Empieza la tela \n");

	while(1){
	// lee los 255 caracteres... o no
			int a=0;
			for (a=0;a<50;a++){ // lo lee hasta 255 caracteres maximo
				read(serial_port,recibido,1); //OJO se para hasta que lea algo
				//printf("a %i\n",a);
				if (recibido[0]==13){
				buf[a]='\n'; //Caracter retorno del carro me interesa
					//printf("recibido retorno de carro en %i\n",a);
				}
				if (recibido[0]==10){ // Caracter de line feed corta la historia
					buf[a]='\n'; //Parece ser que hay que enviar dos retornos de carro
					break;
				}
				if(recibido[0]>31&&recibido[0]<127){
					buf[a]=recibido[0];
					//printf("Recibido %i\n",recibido[0]);
				}					
			} //termina el for ya tengo una cadena		
			//He recibido la orden del arduino o no
			//printf("Termina el for a:%i\n",a);
			//Envio al socket
		//printf("empieza el if que mira el buffer %i\n",buf[1]);
		if (buf[1]>31 ){ //Si el buffer tiene datos
			printf("Buffer :%s\n",buf);			
			write(sockfd,buf,a+1); //PArece ser que le sientan mal los espacios??
			//printf("tamaño %i\n",sizeof(buf)); ///***************
			bzero(&buf,sizeof(buf));
		}

	//read(serial_port,recibido,20);//No funciona bien, ni coge todos los caracteres
	//ni quita caracteres basura. No se si será del protocolo o que
	//printf("%s \n",recibido);
  }
	
	//for (int cont;cont<255;cont++){printf("Z");};

	//write(serial_port,buf2,8);
	//write(serial_port,buf2,sizeof(buf2));
	//printf("Buf2 : %s\n",buf2);
	//write(serial_port,buf,sizeof(buf));
	//printf("Buf : %s\n",buf);

	//write(serial_port,"A",1);
	//printf(serial_port,"a",1);

	//while (sigue==1){
		//res = read(serial_port,buf,255);
		//printf(res);
		//printf("\n");
		//printf (buf[0]);
		//printf("\n");
		//if (buf[0]=="z"){sigue = 0;}
	//}

	close(serial_port);
	return(0);
}

void logosound(){
	write(sockfd,"noteon 9 38 127\n",16);
	usleep(700000);
	write(sockfd,"noteon 9 50 127\n",16);
	usleep(700000);
	write(sockfd,"noteon 9 47 127\n",16);
	usleep(600000);
	write(sockfd,"noteon 9 45 127\n",16);
	write(sockfd,"noteon 9 55 127\n",16);

}


